---
layout: printabledoc
permalink: /document
---

{{ toc }}

{% include que_es.md %}

{% include estrategies.md %}

# Segueix-nos!
<div class="social" markdown="1" style="page-break-after: always;">

{% include social.md %}

</div>


<div class="social" markdown="1" style="margin:3cm;">
Aquest document ha estat imprès el dia {% last_modified_at %d/%m/%Y %}

Distribuir sota llicència de domini públic, CC0: https://creativecommons.org/publicdomain/zero/1.0/

![CC0](https://licensebuttons.net/p/zero/1.0/80x15.png)
</div>
