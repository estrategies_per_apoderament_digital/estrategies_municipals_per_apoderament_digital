#### subject:
Invitació a la presentació de les Estratègies Municipals per a l'Apoderament Digital

#### body:
Bon dia,

Us escrivim per convidar-vos a la presentació de les [Estratègies Municipals per a l'Apoderament Digital](https://apoderamentdigital.cat), una proposta elaborada conjuntament entre una dotzena d'entitats (Sobtec, Barcelona Free Software, Pangea, Colectic, Pirates de Catalunya, Xarxa d'Innovació Pública, GufiNet/Exo, The Things Network Catalunya, Fem Procomuns, Free Knowledge Institute) que incorpora 44 mesures per a l'apoderament tecnològic als municipis. L'objectiu és donar-les a conèixer entre les entitats del sector, però també fer-les arribar als partits polítics que concorreran a les eleccions municipals del 2019, per tal que les incorporin als seus programes electorals, mirant d'avançar, així, en la construcció de sobirania tecnològica, també des dels municipis. Com veureu, la guia recull aquestes 44 mesures, però també objectius i línies d'actuació concretes, i estem treballant en un seguit d'eines i recursos per a facilitar la posada en pràctica de les diverses propostes.

L'acte serà dijous 29 de novembre, a les 19h, a l'espai [Inceptum](https://inceptum.org/) (c/ del Rector Triadó, 31, 08014 Barcelona). Ens fa molta il·lusió donar a conèixer públicament el projecte i fer-ho, especialment, entre la comunitat tecnològica que, d'una manera o altra, fa temps que treballem per a fer realitat moltes de les propostes d'aquestes Estratègies. És per això que ens agradaria poder comptar amb la vostra presència, per visibilitzar la feina feta i per generar-nos un espai de trobada i celebració (hi haurà un petit refrigeri al final).

A partir del mes de desembre, iniciarem els contactes amb els partits polítics i farem un seguiment de les trobades, per veure on s'incorporen les propostes, i si s'acaben desenvolupant o no. Com la resta del procés, es tractarà d'un espai obert a tothom qui vulgui participar. Per a formar-ne part o per a més informació, podeu contactar-nos a info@apoderamentdigital.cat. Igualment, si no sou [entitat impulsora ni contribuïdora](https://apoderamentdigital.cat/com_ho_estem_fent/), però voleu donar públicament el vostre suport a la guia, escriviu-nos per afegir-vos al llistat de simpatitzants que penjarem properament al web.

Fins aviat,
