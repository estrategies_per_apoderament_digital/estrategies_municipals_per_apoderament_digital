>%nobreak%
> ## Impulsores
> Els impulsors d'aquest projecte ens reunim periòdicament per debatre les aportacion fetes, parlar de les seves mancances, així com  promoure la seva projecció pública. Per demanar més informació sobre aquestes reunions pots enviar un correu a [info@apoderamentdigital.cat](mailto:info@apoderamentdigital.cat).

> * Sobtec - http://sobtec.cat
> * Barcelona Free Software - https://bcnfs.org
> * Pangea - https://pangea.org
> * Colectic - https://colectic.coop
> * Pirates de Catalunya - http://pirata.cat
> * Xarxa Innovació Pública - http://www.xarxaip.cat
> * GuifiNet/Exo - https://guifi.net

>%nobreak%
> ## Contribuidores
>
> Els contribuidors s'encarreguen de fer propostes específiques per ampliar els diferents apartats de la guia.
>
> * The Things Network Catalunya
> * FemProcomuns
> * Free Knowledge Institute
